<?php

namespace Blinkio\KipBundle\Annotation\Doctrine;

/**
 * Class Association
 *
 * @package Blinkio\KipBundle\Annotation\Doctrine
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 *
 * @Annotation
 */
class Association
{
    /**
     * @var \Doctrine\ORM\Mapping\Column
     */
    public $column;

    /**
     * @var array
     */
    public $parameterMapping = [];

    /**
     * @var string
     */
    public $targetClass;

    /**
     * @var array
     */
    public $operations = [];

    /**
     * @var string
     */
    public $authenticationStrategy;

    /**
     * @var bool
     */
    public $isResultCacheEnabled = true;

    /**
     * @var int
     */
    public $resultCacheTimeout = null;
}
