<?php

namespace Blinkio\KipBundle\Annotation;

/**
 * Class Id
 *
 * @package Blinkio\KipBundle\Annotation
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 *
 * @Annotation
 */
class Id
{

}
