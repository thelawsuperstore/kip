<?php

namespace Blinkio\KipBundle\Annotation;

/**
 * Class Mapping
 *
 * @package Blinkio\KipBundle\Annotation
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 *
 * @Annotation
 */
class Mapping
{
    /**
     * @var string
     */
    public $method;

    /**
     * @var string
     */
    public $uri;
}
