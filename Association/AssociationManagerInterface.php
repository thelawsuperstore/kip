<?php

namespace Blinkio\KipBundle\Association;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Interface AssociationManagerInterface
 *
 * @package Blinkio\KipBundle\Association
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
interface AssociationManagerInterface
{
    /**
     * Perform mapping operations, etc.
     *
     * @param ClassMetadata $meta
     * @return void
     */
    public function loadClassMetaData(ClassMetadata $meta);

    /**
     * Perform load operations on an entity's Kip associations
     *
     * @param object $entity
     * @return void
     */
    public function load($entity);

    /**
     * Perform persist operations on an entity's Kip associations
     *
     * @param LifecycleEventArgs $args
     * @return void
     */
    public function persist(LifecycleEventArgs $args);

    /**
     * Perform update operations on an entity's Kip associations
     *
     * @param PreUpdateEventArgs $args
     * @return void
     */
    public function update(PreUpdateEventArgs $args);

    /**
     * Perform remove operations on an entity's Kip associations
     *
     * @param object $entity
     * @return void
     */
    public function remove($entity);
}
