<?php

namespace Blinkio\KipBundle\Association;

use Blinkio\KipBundle\Annotation\Doctrine\Association;
use Blinkio\KipBundle\Exception\Http\AbstractHttpException;
use Blinkio\KipBundle\Manager\ManagerInterface;
use Doctrine\Common\Annotations\Reader as AnnotationReaderInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\Column;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Class DoctrineAssociationManager
 *
 * @package Blinkio\KipBundle\Association
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class DoctrineAssociationManager extends ContainerAware implements AssociationManagerInterface
{
    /**
     * Association annotation clas
     */
    const ANNOTATION_ASSOCIATION = 'Blinkio\KipBundle\Annotation\Doctrine\Association';

    /**
     * @var AnnotationReaderInterface
     */
    private $annotationReader;

    /**
     * @var ManagerInterface
     */
    private $kip;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Association[]
     */
    private static $associationAnnotations = [];

    /**
     * Constructor
     *
     * @param AnnotationReaderInterface $annotationReader
     * @param ManagerInterface $kip
     * @param LoggerInterface $logger
     */
    public function __construct(AnnotationReaderInterface $annotationReader, ManagerInterface $kip, LoggerInterface $logger)
    {
        $this->annotationReader = $annotationReader;
        $this->kip = $kip;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function loadClassMetaData(ClassMetadata $meta)
    {
        foreach ($this->getAnnotations($meta->getName()) as $property => $annotation) {
            if ($annotation->column instanceof Column) {
                if (! $meta->isInheritedField($property)) {
                    $meta->mapField(
                        array_merge(['fieldName' => $property, 'columnName' => $annotation->column->name], (array) $annotation->column));
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function load($entity)
    {
        foreach ($this->getAnnotations(get_class($entity)) as $property => $annotation) {
            $this->doLoad($annotation, $entity, $property);
        }
    }

    /**
     * Perform the load operation
     *
     * @param Association $annotation
     * @param object $entity
     * @param string $property
     */
    private function doLoad(Association $annotation, $entity, $property)
    {
        if (! isset($annotation->operations['load'])) {
            return;
        }

        if (! $annotation->targetClass) {
            throw new \RuntimeException(
                sprintf('Attribute "targetClass" has not been specified on property %s::%s', get_class($entity), $property));
        }

        $reflectionProperty = (new \ReflectionObject($entity))->getProperty($property);
        if (! $reflectionProperty) {
            throw new \RuntimeException(
                sprintf('Failed to find property "%s" on entity "%s"', $property, get_class($entity)));
        }

        $reflectionProperty->setAccessible(true);

        $model = $this->instantiateModel($annotation->targetClass);

        if ($annotation->authenticationStrategy && $this->container->has($annotation->authenticationStrategy)) {
            $this->kip->setAuthenticationStrategy(
                $this->container->get($annotation->authenticationStrategy)
            );
        }

        $fkParameters = [];
        if ($annotation->column instanceof Column) {
            if (isset($annotation->parameterMapping[$property])) {
                $fkParameters[$property] = $annotation->parameterMapping[$property];
            } else {
                throw new \RuntimeException(
                    sprintf('Parameter mapping not defined for association, please add "parameterMapping={"%s"="<kip_parameter>"}" '.
                        'to the annotation for the property "%s" on entity "%s", where the left side is the local property and the right side is the Kip parameter', $property, $property, get_class($entity)));
            }
        }

        $model = $this->performKipTransaction($model, $entity, array_merge($fkParameters, $annotation->parameterMapping), $annotation);

        if (! $this->isNulledObject($model)) {
            $reflectionProperty->setValue($entity, $model);
        }
    }

    /**
     * Perform the Kip transaction
     *
     * @param object $model
     * @param object $entity
     * @param array $parameters
     * @param Association $annotation
     * @return object
     */
    private function performKipTransaction($model, $entity, array $parameters, Association $annotation)
    {
        $parameters = $this->buildGetParameters($entity, $parameters);
        $method = $annotation->operations['load'];

        if (null === current($parameters)) {
            return null;
        }

        try {
            $model = $this->kip->send(
                $model,
                $method,
                $parameters,
                null,
                ['result_cache_enabled' => $annotation->isResultCacheEnabled, 'result_cache_timeout' => $annotation->resultCacheTimeout]
            );
        } catch (AbstractHttpException $e) {
            $this->logger->error(sprintf(
                'Kip encountered an error of type "%s" when trying to load a resource, resulting in the status code "%s"',
                get_class($e),
                $e->getResponse()->getStatusCode()
            ));
        }

        return $model;
    }

    /**
     * Are all properties of the object NULL?
     *
     * @param object $object
     * @return bool
     */
    private function isNulledObject($object)
    {
        if (! isset($object->__isEmpty)) {
            return true;
        }

        return $object->__isEmpty;
    }

    /**
     * Instantiate model (without calling constructor)
     *
     * @param string $class
     * @return object
     */
    private function instantiateModel($class)
    {
        return (new \ReflectionClass($class))->newInstanceWithoutConstructor();
    }

    /**
     * Build URL parameters for Kip from entity property mapping
     *
     * @param object $entity
     * @param array $mapping
     * @return array
     */
    private function buildGetParameters($entity, $mapping)
    {
        $reflection = new \ReflectionObject($entity);

        $urlParameters = [];
        foreach ($mapping as $propertyName => $urlParameter) {
            $prop = $reflection->getProperty($propertyName);
            if ($prop) {
                $prop->setAccessible(true);
                $urlParameters[$urlParameter] = $prop->getValue($entity);
            }
        }

        return $urlParameters;
    }

    /**
     * {@inheritdoc}
     */
    public function persist(LifecycleEventArgs $args)
    {
        $this->performPersist($args->getEntity());
    }

    /**
     * {@inheritdoc}
     */
    public function update(PreUpdateEventArgs $args)
    {
        $this->performPersist($args->getEntity());
    }

    /**
     * {@inheritdoc}
     */
    public function remove($entity)
    {
        // todo: implement remove method
        throw new \BadMethodCallException('Method "remove" has not been implemented');
    }

    /**
     * Get association annotations for class properties
     *
     * @param string $class
     * @return array
     */
    private function getAnnotations($class)
    {
        if (isset(self::$associationAnnotations[$class])) {
            return self::$associationAnnotations[$class];
        }

        $annotations = [];
        foreach ((new \ReflectionClass($class))->getProperties() as $property) {
            if ($annotation = $this->annotationReader->getPropertyAnnotation($property, self::ANNOTATION_ASSOCIATION)) {
                $annotations[$property->getName()] = $annotation;
            }
        }

        return self::$associationAnnotations[$class] = $annotations;
    }

    /**
     * Perform persist/update operation
     *
     * @param object $entity
     */
    private function performPersist($entity)
    {
        $entityClass = get_class($entity);

        foreach ($this->getAnnotations($entityClass) as $property => $annotation) {
            if ($annotation->column && is_array($annotation->parameterMapping)) {
                if (count($annotation->parameterMapping) > 1) {
                    throw new \RuntimeException(
                        sprintf('Kip association on property "%s" in class "%s" must only have one parameter mapping, multiple given', $property, $entityClass));
                }

                $property = new \ReflectionProperty($entityClass, $property);
                $property->setAccessible(true);
                $value = $property->getValue($entity);

                if (is_object($value) && ! ($value instanceof \Traversable)) {
                    $propertyObject = new \ReflectionObject($value);

                    $targetProperty = $propertyObject->getProperty(current($annotation->parameterMapping));
                    if ($targetProperty) {
                        $targetProperty->setAccessible(true);
                        $targetPropertyValue = $targetProperty->getValue($value);

                        $property->setValue($entity, $targetPropertyValue);
                    }
                }
            }
        }
    }
}
