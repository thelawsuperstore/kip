<?php

namespace Blinkio\KipBundle\Authentication\Strategy;

use Psr\Http\Message\RequestInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AbstractAuthenticationStrategy
 *
 * @package Blinkio\KipBundle\Authentication\Strategy
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
abstract class AbstractAuthenticationStrategy implements AuthenticationStrategyInterface
{
    /**
     * @var array
     */
    protected $parameters;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $parameters = [])
    {
        $resolver = new OptionsResolver();
        $this->configureParametersResolver($resolver);

        $this->parameters = $resolver->resolve($parameters);
    }

    /**
     * {@inheritdoc}
     */
    public function modifyRequest(RequestInterface $request)
    {
    }

    /**
     * Configure options resolver for parameters
     *
     * @param OptionsResolver $resolver
     */
    protected function configureParametersResolver(OptionsResolver $resolver)
    {
    }
}
