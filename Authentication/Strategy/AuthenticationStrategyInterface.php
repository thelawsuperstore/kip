<?php

namespace Blinkio\KipBundle\Authentication\Strategy;

use Psr\Http\Message\RequestInterface;

/**
 * Interface AuthenticationStrategyInterface
 *
 * @package Blinkio\KipBundle\Authentication\Strategy
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
interface AuthenticationStrategyInterface
{
    /**
     * Get the name of this authentication strategy
     *
     * @return string
     */
    public function getName();

    /**
     * Modify the request in some way to implement the
     * authentication strategy
     *
     * @param RequestInterface $request
     * @return RequestInterface
     */
    public function modifyRequest(RequestInterface $request);
}
