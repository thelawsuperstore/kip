<?php

namespace Blinkio\KipBundle\Authentication\Strategy;

use Psr\Http\Message\RequestInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class WsseAuthenticationStrategy
 *
 * @package Blinkio\KipBundle\Authentication\Strategy
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class WsseAuthenticationStrategy extends AbstractAuthenticationStrategy
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'wsse';
    }

    /**
     * {@inheritdoc}
     */
    public function modifyRequest(RequestInterface $request)
    {
        return $request
            ->withHeader('Authorization', sprintf('WSSE profile="%s"', $this->parameters['profile']))
            ->withHeader('X-WSSE', $this->buildWsseHeader());
    }

    /**
     * {@inheritdoc}
     */
    protected function configureParametersResolver(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired([
                'username',
                'key',
            ])
            ->setDefaults([
                'time_offset_seconds' => 0,
                'profile' => 'UsernameToken',
            ]);
    }

    /**
     * Build the WSSE header
     *
     * @return string
     */
    protected function buildWsseHeader()
    {
        $prefix = gethostname();
        $nonce = base64_encode(substr(md5(uniqid($prefix.'_', true)), 0, 16));
        $created = date('c', strtotime($this->parameters['time_offset_seconds'].' SECONDS'));
        $digest = base64_encode(sha1(base64_decode($nonce).$created.$this->parameters['key'], true));

        return sprintf(
            'UsernameToken Username="%s", PasswordDigest="%s", Nonce="%s", Created="%s"',
            $this->parameters['username'],
            $digest,
            $nonce,
            $created
        );
    }
}
