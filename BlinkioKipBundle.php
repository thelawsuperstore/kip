<?php

namespace Blinkio\KipBundle;

use Blinkio\KipBundle\DependencyInjection\Compiler\ExceptionDispatcherCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class BlinkioKipBundle
 *
 * @package Blinkio\KipBundle
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class BlinkioKipBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new ExceptionDispatcherCompilerPass());
    }
}
