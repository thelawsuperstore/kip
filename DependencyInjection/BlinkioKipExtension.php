<?php

namespace Blinkio\KipBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

/**
 * Class BlinkioKipExtension
 *
 * @package Blinkio\KipBundle\DependencyInjection
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class BlinkioKipExtension extends ConfigurableExtension
{
    /**
     * {@inheritdoc}
     */
    protected function loadInternal(array $mergedConfig, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(array(__DIR__.'/../Resources/config/')));
        $loader->load('services.yml');

        $container->setParameter('blinkio.kip.config.api_uri_prefix', $mergedConfig['api_uri_prefix']);
        $container->setParameter('blinkio.kip.config.mappings', isset($mergedConfig['mappings']) ? $mergedConfig['mappings'] : []);
        $container->setParameter('blinkio.kip.config.timeout_seconds', $mergedConfig['timeout_seconds']);
        $container->setParameter('blinkio.kip.config.content_type', $mergedConfig['content_type']);

        if (isset($mergedConfig['result_cache']['cache_service'])) {
            $container->setAlias('blinkio.kip.result_cache.cache_service', $mergedConfig['result_cache']['cache_service']);
        }

        $container->setParameter(
            'blinkio.kip.result_cache.default_timeout_seconds',
            (isset($mergedConfig['result_cache']['default_timeout_seconds']) ? $mergedConfig['result_cache']['default_timeout_seconds'] : null)
        );

        $container->setParameter(
            'blinkio.kip.result_cache.security_token_aware',
            (isset($mergedConfig['result_cache']['security_token_aware']) ? $mergedConfig['result_cache']['security_token_aware'] : false)
        );

        $container->setDefinition('blinkio.kip.http_client', new Definition(
            'GuzzleHttp\Client', [
                ['base_uri' => $this->fixSlashesForUriPrefix($mergedConfig['api_uri_prefix'])]
            ]
        ));
    }

    /**
     * Fixes slashes for RFC 3986 compliance
     *
     * @param string $uriPrefix
     * @return string
     */
    protected function fixSlashesForUriPrefix($uriPrefix)
    {
        if ('/' == substr($uriPrefix, -1)) {
            return $uriPrefix;
        }

        return sprintf('%s/', $uriPrefix);
    }
}
