<?php

namespace Blinkio\KipBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class ExceptionDispatcherCompilerPass
 *
 * @package Blinkio\KipBundle\DependencyInjection\Compiler
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class ExceptionDispatcherCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (! $container->hasDefinition('blinkio.kip.exception.http_exception_dispatcher')) {
            return;
        }

        $definition = $container->getDefinition(
            'blinkio.kip.exception.http_exception_dispatcher'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'blinkio.kip.http_exception'
        );

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'addException',
                    array(new Reference($id), isset($attributes['priority']) ? $attributes['priority'] : 0)
                );
            }
        }
    }
}
