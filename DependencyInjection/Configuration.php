<?php

namespace Blinkio\KipBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @package Blinkio\KipBundle\DependencyInjection
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $tb = new TreeBuilder();
        $root = $tb->root('blinkio_kip');

        $root
            ->children()
                ->scalarNode('content_type')->end()
                ->integerNode('timeout_seconds')->end()
                ->scalarNode('api_uri_prefix')->end()
                ->variableNode('mappings')->end()
                ->variableNode('model_directories')->end()
                ->variableNode('result_cache')->end()
            ->end();

        return $tb;
    }
}
