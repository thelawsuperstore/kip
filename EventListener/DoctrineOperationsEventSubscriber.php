<?php

namespace Blinkio\KipBundle\EventListener;

use Blinkio\KipBundle\Association\AssociationManagerInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class DoctrineOperationsEventSubscriber
 *
 * @package Blinkio\KipBundle\EventListener
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class DoctrineOperationsEventSubscriber implements EventSubscriber
{
    /**
     * @var AssociationManagerInterface
     */
    private $associationManager;

    /**
     * Constructor
     *
     * @param AssociationManagerInterface $associationManager
     */
    public function __construct(AssociationManagerInterface $associationManager)
    {
        $this->associationManager = $associationManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
            Events::postLoad,
            Events::preUpdate,
            Events::prePersist,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $args)
    {
        $this->associationManager->loadClassMetaData($args->getClassMetadata());
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $this->associationManager->load($args->getEntity());
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->associationManager->persist($args);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $this->associationManager->update($args);
    }
}