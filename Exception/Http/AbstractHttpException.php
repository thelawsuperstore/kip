<?php

namespace Blinkio\KipBundle\Exception\Http;

use Psr\Http\Message\ResponseInterface;

/**
 * Class AbstractHttpException
 *
 * @package Blinkio\KipBundle\Exception\Http
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
abstract class AbstractHttpException extends \RuntimeException
{
    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * Constructor
     */
    final public function __construct()
    {
        parent::__construct($this->getLabel());
    }

    /**
     * Get response
     *
     * @return ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Initialise this exception with a Response object
     *
     * @param ResponseInterface $response
     * @return $this
     */
    public function initialise(ResponseInterface $response)
    {
        $this->onInitialise($response);

        $this->response = $response;

        return $this;
    }

    /**
     * Return TRUE if this exception can be fired. For example, in
     * a subclass of this that represents a not found exception
     *
     * <code>
     * public function canDispatch(Response $response)
     * {
     *     return (404 == $request->getStatusCode());
     * }
     * </code>
     *
     * @param ResponseInterface $response
     * @return bool
     */
    abstract public function canDispatch(ResponseInterface $response);

    /**
     * Get a label for this exception (sed for exception reporting)
     *
     * <code>
     * public function getLabel()
     * {
     *     return 'Not Found';
     * }
     * </code>
     *
     * @return string
     */
    abstract public function getLabel();

    /**
     * Hook into the initialisation of this exception
     *
     * @param ResponseInterface $response
     */
    protected function onInitialise(ResponseInterface $response)
    {
    }
}
