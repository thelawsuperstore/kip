<?php

namespace Blinkio\KipBundle\Exception\Http;

use Psr\Http\Message\ResponseInterface;

/**
 * Class AuthorizationFailureException
 *
 * @package Blinkio\KipBundle\Exception\Http
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class AuthorizationFailureException extends AbstractHttpException
{
    /**
     * {@inheritdoc}
     */
    public function canDispatch(ResponseInterface $response)
    {
        return (401 == $response->getStatusCode());
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        return 'Authorization Failure';
    }
}
