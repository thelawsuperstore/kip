<?php

namespace Blinkio\KipBundle\Exception\Http;

use Psr\Http\Message\ResponseInterface;

/**
 * Class DeserializationException
 *
 * @package Blinkio\KipBundle\Exception\Http
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class DeserializationException extends AbstractHttpException
{
    /**
     * {@inheritdoc}
     */
    public function canDispatch(ResponseInterface $response)
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        return 'Deserialization Exception - Malformed Response';
    }
}
