<?php

namespace Blinkio\KipBundle\Exception\Http;

use Psr\Http\Message\ResponseInterface;

/**
 * Class InternalServerErrorException
 *
 * @package Blinkio\KipBundle\Exception\Http
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class InternalServerErrorException extends AbstractHttpException
{
    /**
     * {@inheritdoc}
     */
    public function canDispatch(ResponseInterface $response)
    {
        return (500 == $response->getStatusCode());
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        return 'Internal Server Error';
    }
}