<?php

namespace Blinkio\KipBundle\Exception\Http;

use Psr\Http\Message\ResponseInterface;

/**
 * Class NotFoundException
 *
 * @package Blinkio\KipBundle\Exception\Http
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class NotFoundException extends AbstractHttpException
{
    /**
     * {@inheritdoc}
     */
    public function canDispatch(ResponseInterface $response)
    {
        return (404 == $response->getStatusCode());
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        return 'Resource Not Found';
    }
}
