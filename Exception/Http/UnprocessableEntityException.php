<?php

namespace Blinkio\KipBundle\Exception\Http;

use Psr\Http\Message\ResponseInterface;

/**
 * Class UnprocessableEntityException
 *
 * @package Blinkio\KipBundle\Exception\Http
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class UnprocessableEntityException extends AbstractHttpException
{
    /**
     * {@inheritdoc}
     */
    public function canDispatch(ResponseInterface $response)
    {
        return (422 == $response->getStatusCode());
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        return 'Unprocessable Entity';
    }
}
