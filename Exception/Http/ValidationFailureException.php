<?php

namespace Blinkio\KipBundle\Exception\Http;

use Psr\Http\Message\ResponseInterface;

/**
 * Class ValidationFailureException
 *
 * @package Blinkio\KipBundle\Exception\Http
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class ValidationFailureException extends AbstractHttpException
{
    /**
     * @var array
     */
    protected $errors = [
        'errors' => [],
    ];

    /**
     * {@inheritdoc}
     */
    public function canDispatch(ResponseInterface $response)
    {
        return (400 == $response->getStatusCode());
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        return 'Validation Failure';
    }

    /**
     * Get errors
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * {@inheritdoc}
     */
    protected function onInitialise(ResponseInterface $response)
    {
        $body = json_decode((string) $response->getBody(), true);

        if ($body && isset($body['errors'])) {
            $this->errors['errors'] = $body['errors'];
        }
    }
}
