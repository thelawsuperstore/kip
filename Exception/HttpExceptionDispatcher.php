<?php

namespace Blinkio\KipBundle\Exception;

use Blinkio\KipBundle\Exception\Http\AbstractHttpException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class HttpExceptionDispatcher
 *
 * @package Blinkio\KipBundle\Exception
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class HttpExceptionDispatcher implements HttpExceptionDispatcherInterface
{
    /**
     * @var AbstractHttpException[]
     */
    protected $exceptions = [];

    /**
     * {@inheritdoc}
     */
    public function dispatch(ResponseInterface $response)
    {
        ksort($this->exceptions);
        $this->exceptions = array_reverse($this->exceptions);

        foreach ($this->exceptions as $exception) {
            if ($exception->canDispatch($response)) {
                throw $exception->initialise($response);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addException(AbstractHttpException $exception, $priority)
    {
        $this->exceptions[sprintf('%s_%s', $priority, count($this->exceptions))] = $exception;

        return $this;
    }
}
