<?php

namespace Blinkio\KipBundle\Exception;

use Blinkio\KipBundle\Exception\Http\AbstractHttpException;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface HttpExceptionDispatcherInterface
 *
 * @package Blinkio\KipBundle\Exception
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
interface HttpExceptionDispatcherInterface
{
    /**
     * Dispatch an exception based on HTTP response code
     *
     * @param ResponseInterface $response
     * @return void
     * @throws AbstractHttpException
     */
    public function dispatch(ResponseInterface $response);

    /**
     * Add an exception to the dispatcher
     *
     * @param AbstractHttpException $exception
     * @param int $priority
     */
    public function addException(AbstractHttpException $exception, $priority);
}
