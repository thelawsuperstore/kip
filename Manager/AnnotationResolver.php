<?php

namespace Blinkio\KipBundle\Manager;

use Blinkio\KipBundle\Annotation\Mapping;
use Doctrine\Common\Annotations\Reader as AnnotationReaderInterface;

/**
 * Class AnnotationResolver
 *
 * @package Blinkio\KipBundle\Manager
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class AnnotationResolver implements AnnotationResolverInterface
{
    /**
     * @var AnnotationReaderInterface
     */
    private $annotationReader;

    /**
     * Constructor
     *
     * @param AnnotationReaderInterface $annotationReader
     */
    public function __construct(AnnotationReaderInterface $annotationReader)
    {
        $this->annotationReader = $annotationReader;
    }

    /**
     * {@inheritdoc}
     */
    public function getIdProperties($object)
    {
        $idProperties = [];
        foreach ((new \ReflectionObject($object))->getProperties() as $property) {
            if ($this->annotationReader->getPropertyAnnotation($property, 'Blinkio\KipBundle\Annotation\Id')) {
                $idProperties[] = $property->getName();
            }
        }

        return $idProperties;
    }

    /**
     * {@inheritdoc}
     */
    public function getMappingsForModelObject($object)
    {
        $class = get_class($object);
        $mappings = [];
        foreach ($this->annotationReader->getClassAnnotations(new \ReflectionClass($class)) as $annotation) {
            if ($annotation instanceof Mapping) {
                $mappings[] = [
                    'method' => $annotation->method,
                    'uri' => $annotation->uri,
                    'class' => $class,
                ];
            }
        }

        return $mappings;
    }
}
