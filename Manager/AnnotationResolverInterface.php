<?php

namespace Blinkio\KipBundle\Manager;

/**
 * Interface AnnotationResolverInterface
 *
 * @package Blinkio\KipBundle\Manager
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
interface AnnotationResolverInterface
{
    /**
     * Get a list of Kip Id properties based on
     * the @see Blinkio\KipBundle\Annotation\Id annotation
     *
     * @param object $object
     * @return array
     */
    public function getIdProperties($object);

    /**
     * Get a list of mappings for a given model class
     *
     * @param object $object
     * @return array
     */
    public function getMappingsForModelObject($object);
}
