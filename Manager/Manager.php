<?php

namespace Blinkio\KipBundle\Manager;

use Blinkio\KipBundle\Authentication\Strategy\AuthenticationStrategyInterface;
use Blinkio\KipBundle\Exception\Http\AbstractHttpException;
use Blinkio\KipBundle\Exception\Http\DeserializationException;
use Blinkio\KipBundle\Exception\HttpExceptionDispatcherInterface;
use Blinkio\KipBundle\Exception\Http\AuthorizationFailureException;
use Doctrine\Common\Cache\Cache;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class Manager
 *
 * @package Blinkio\KipBundle\Manager
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
class Manager implements ManagerInterface
{
    /**
     * @var array
     */
    private static $mutableHttpMethods = [
        'POST',
        'PUT',
        'PATCH',
        'DELETE',
    ];

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var array
     */
    private $mappings;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var AnnotationResolverInterface
     */
    private $annotationResolver;

    /**
     * @var AuthenticationStrategyInterface
     */
    private $authStrategy;

    /**
     * @var int
     */
    private $timeoutSeconds;

    /**
     * @var string
     */
    private $contentType;

    /**
     * @var HttpExceptionDispatcherInterface
     */
    private $exceptionDispatcher;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Cache
     */
    private $resultCache;

    /**
     * @var int|null
     */
    private $resultCacheTimeoutSeconds;

    /**
     * @var bool
     */
    private $resultCacheSecurityTokenAware;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * Constructor
     *
     * @param LoggerInterface $logger
     * @param ClientInterface $client
     * @param SerializerInterface $serializer
     * @param AnnotationResolverInterface $annotationResolver
     * @param HttpExceptionDispatcherInterface $exceptionDispatcher
     * @param string $contentType
     * @param int $timeoutSeconds
     * @param array $mappings
     * @param TokenStorageInterface $tokenStorage
     * @param Cache $resultCache
     * @param int|null $resultCacheTimeoutSeconds
     * @param bool $resultCacheSecurityTokenAware
     */
    public function __construct(
        LoggerInterface $logger,
        ClientInterface $client,
        SerializerInterface $serializer,
        AnnotationResolverInterface $annotationResolver,
        HttpExceptionDispatcherInterface $exceptionDispatcher,
        $contentType,
        $timeoutSeconds,
        array $mappings,
        TokenStorageInterface $tokenStorage,
        Cache $resultCache = null,
        $resultCacheTimeoutSeconds = null,
        $resultCacheSecurityTokenAware = false)
    {
        $this->serializer = $serializer;
        $this->mappings = $mappings;
        $this->client = $client;
        $this->annotationResolver = $annotationResolver;
        $this->timeoutSeconds = $timeoutSeconds;
        $this->contentType = $contentType;
        $this->exceptionDispatcher = $exceptionDispatcher;
        $this->logger = $logger;
        $this->resultCache = $resultCache;
        $this->resultCacheTimeoutSeconds = $resultCacheTimeoutSeconds;
        $this->resultCacheSecurityTokenAware = $resultCacheSecurityTokenAware;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function setAuthenticationStrategy(AuthenticationStrategyInterface $authStrategy)
    {
        $this->authStrategy = $authStrategy;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function get($object, array $parameters = [], $overrideInput = null, array $options = [])
    {
        return $this->send($object, 'GET', $parameters, $overrideInput, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function post($object, array $parameters = [], $overrideInput = null, array $options = [])
    {
        return $this->send($object, 'POST', $parameters, $overrideInput, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function put($object, array $parameters = [], $overrideInput = null, array $options = [])
    {
        return $this->send($object, 'PUT', $parameters, $overrideInput, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function delete($object, array $parameters = [], $overrideInput = null, array $options = [])
    {
        return $this->send($object, 'DELETE', $parameters, $overrideInput, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function patch($object, array $parameters = [], $overrideInput = null, array $options = [])
    {
        return $this->send($object, 'PATCH', $parameters, $overrideInput, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function head($object, array $parameters = [], $overrideInput = null, array $options = [])
    {
        return $this->send($object, 'HEAD', $parameters, $overrideInput, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function send($object, $method, array $parameters = [], $overrideInput = null, array $options = [])
    {
        $output = $object;

        if (null !== $overrideInput) {
            $object = $overrideInput;
        }

        $mappings = $this->buildMappingsFor($object);

        $class = get_class($object);
        foreach ($mappings as $mapping) {
            if ($mapping['class'] == $class && strtoupper($mapping['method']) == strtoupper($method)) {
                break;
            }
        }

        if (! isset($mapping)) {
            throw new \RuntimeException(sprintf(
                'Mapping could not be found for class %s using method %s', $class, $method));
        }

        $uri = ltrim($mapping['uri'], '/');

        $pathParameters = [];
        $hasParams = false;
        if (preg_match_all('/{+(.*?)}/i', $uri, $pathParameters)) {
            unset($pathParameters[0]);
            if (isset($pathParameters[1]) && is_array($pathParameters[1])) {
                $pathParameters = array_values($pathParameters[1]);
                $hasParams = true;
            }
        }

        if (! $hasParams) {
            $pathParameters = [];
        }

        $pathParameters = array_flip($pathParameters);

        foreach ($pathParameters as $key => $param) {
            if (isset($parameters[$key])) {
                $pathParameters[$key] = $parameters[$key];
                unset($parameters[$key]);
            } else {
                $accessor = sprintf('get%s', ucfirst($key));
                if (method_exists($object, $accessor)) {
                    $pathParameters[$key] = $object->$accessor();
                }
            }
        }

        foreach ($this->annotationResolver->getIdProperties($object) as $idProperty) {
            $mutator = sprintf('set%s', ucfirst($idProperty));
            if (method_exists($object, $mutator)) {
                $object->$mutator(null);
            }
        }

        foreach ($pathParameters as $k => $v) {
            $uri = str_replace('{'.$k.'}', $v, $uri, $count);
        }

        if (count($parameters)) {
            $uri = sprintf('%s?%s', $uri, http_build_query($parameters));
        }

        $encoding = $this->getEncodingFromContentMimeType($this->contentType);
        if (! in_array($encoding, ['json', 'xml', 'yml'])) {
            throw new \RuntimeException(sprintf('Encoding %s is not supported', $encoding));
        }

        $response = $this->sendRaw(
            $method,
            $uri,
            (in_array($method, self::$mutableHttpMethods) ? $this->serializer->serialize($object, $encoding) : null),
            $options
        );

        if (null === $response) {
            return null;
        }

        if (204 == $response->getStatusCode()) {
            return '';
        }

        try {
            $output = $this->serializer->deserialize((string) $response->getBody(), get_class($output), $encoding);

            $output->__isEmpty = true;
            if (preg_match('/[243]+\d{2}/', $response->getStatusCode())) {
                $output->__isEmpty = false;
            }
            if (in_array($response->getStatusCode(), [401, 404])) {
                $output->__isEmpty = true;
            }

            return $output;
        } catch (\RuntimeException $e) {
            throw (new DeserializationException())->initialise($response);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function sendRaw($method, $uri, $body = null, array $options = [])
    {
        $isResultCacheEnabled = false;
        if (isset($options['result_cache_enabled']) && $options['result_cache_enabled'] && $this->resultCache) {
            $isResultCacheEnabled = true;
        } elseif (! isset($options['result_cache_enabled']) && $this->resultCache) {
            $isResultCacheEnabled = true;
        }

        $resultCacheKey = $this->buildResultCacheKey($method, $uri, $body, $this->authStrategy);

        if ($this->canResultCache($method) && $isResultCacheEnabled) {
            if ($this->resultCache->contains($resultCacheKey)) {
                list ($resStatusCode, $resHeaders, $resBody) = $this->resultCache->fetch($resultCacheKey);
                $this->logger->debug(
                    sprintf('RESULT CACHE: Kip received response from "%s %s" with status code of "%s" with body: ', $method, $uri, $resStatusCode, $resBody));
                return new Response($resStatusCode, $resHeaders, $resBody);
            }
        }

        $headers = isset($options['headers']) && is_array($options['headers']) ? $options['headers'] : [
            'Accept' => $this->contentType,
            'Content-Type' => $this->contentType,
        ];

        $handlerStack = new HandlerStack();
        $handlerStack->setHandler(new CurlHandler());

        if ($this->authStrategy) {
            $handlerStack->push(Middleware::mapRequest(function (RequestInterface $request) {
                return $this->authStrategy->modifyRequest($request);
            }));
        }

        $this->logger->debug(
            sprintf('Kip sending request to "%s %s" with a body of: %s', $method, $uri, $body));

        try {
            $response = $this->client->request($method, $uri, [
                'headers' => $headers,
                'timeout' => $this->timeoutSeconds,
                'body' => $body,
                'handler' => $handlerStack,
            ]);
        } catch (ClientException $e) {
            $response = $e->getResponse();
        }

        if (! ($response instanceof ResponseInterface)) {
            throw new \RuntimeException(sprintf(
                'Failed to obtain a response for URI %s using method %s', $uri, $method));
        }

        $this->logger->debug(
            sprintf('Kip received response from "%s %s" with status code of "%s" and body of: %s', $method, $uri, $response->getStatusCode(), (string) $response->getBody()));

        if ($this->canResultCache($method) && $isResultCacheEnabled) {
            $this->resultCache->save(
                $resultCacheKey,
                [
                    $response->getStatusCode(),
                    $response->getHeaders(),
                    (string) $response->getBody(),
                ],
                (int) (isset($options['result_cache_timeout']) && null !== $options['result_cache_timeout'] ? $options['result_cache_timeout'] : $this->resultCacheTimeoutSeconds)
            );
        }

        try {
            $this->exceptionDispatcher->dispatch($response);
        } catch (AuthorizationFailureException $e) {
            if ($this->isCli()) {
                return null;
            }
            throw $e;
        }

        return $response;
    }

    /**
     * Get the encoding from a mime type
     *
     * @param string $contentMimeType
     * @return string
     */
    private function getEncodingFromContentMimeType($contentMimeType)
    {
        $parts = explode('/', $contentMimeType);

        return end($parts);
    }

    /**
     * Builds a list of mappings for a given object
     *
     * @param object $object
     * @return array
     * @todo Implement caching
     */
    private function buildMappingsFor($object)
    {
        $mappings = [];
        foreach ($this->mappings as $mapping) {
            if ($mapping['class'] == get_class($object)) {
                $mappings[] = $mapping;
            }
        }

        $mappings = array_merge(
            $mappings,
            $this->annotationResolver->getMappingsForModelObject($object)
        );

        $combined = [];
        foreach ($mappings as $mapping) {
            $combined[md5(serialize($mapping))] = $mapping;
        }

        return array_values($combined);
    }

    /**
     * Is this script invoked by CLI?
     *
     * @return bool
     */
    private function isCli()
    {
        return (strtolower(php_sapi_name()) == "cli");
    }

    /**
     * Can the results from this HTTP method be result cached?
     *
     * @param string $method
     * @return bool
     */
    private function canResultCache($method)
    {
        return ! in_array($method, self::$mutableHttpMethods);
    }

    /**
     * Build a result cache key
     *
     * @param string $method
     * @param string $uri
     * @param string $body
     * @param AuthenticationStrategyInterface|null $authStrategy
     * @return string
     */
    private function buildResultCacheKey($method, $uri, $body, AuthenticationStrategyInterface $authStrategy = null)
    {
        $tokenKey = '';
        if ($this->resultCacheSecurityTokenAware && ($this->tokenStorage->getToken() instanceof TokenInterface)) {
            if ($this->tokenStorage->getToken()->getUser() instanceof UserInterface) {
                $tokenKey = (string) $this->tokenStorage->getToken()->getUser()->getUsername();
            }
        }

        return sha1(serialize($authStrategy).$uri.$tokenKey.$method.serialize($body));
    }
}
