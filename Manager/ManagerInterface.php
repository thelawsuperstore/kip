<?php

namespace Blinkio\KipBundle\Manager;

use Blinkio\KipBundle\Authentication\Strategy\AuthenticationStrategyInterface;

/**
 * Interface ManagerInterface
 *
 * @package Blinkio\KipBundle\Manager
 * @author Ashley Dawson <ashley.dawson@thelawsuperstore.co.uk>
 */
interface ManagerInterface
{
    /**
     * Send a REST request based on a class mapping and method
     *
     * @param object $object
     * @param string $method
     * @param array $parameters
     * @param null|object $overrideInput
     * @param array $options
     * @return object
     */
    public function send($object, $method, array $parameters = [], $overrideInput = null, array $options = []);

    /**
     * Actually perform the request, returning a response or dispatching an
     * error exception
     *
     * @param string $method
     * @param string $uri
     * @param string|null $body
     * @param array $options
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function sendRaw($method, $uri, $body = null, array $options = []);

    /**
     * Set (override) the current authentication strategy
     *
     * @param AuthenticationStrategyInterface $authStrategy
     * @return $this
     */
    public function setAuthenticationStrategy(AuthenticationStrategyInterface $authStrategy);

    /**
     * Perform a GET request on an object
     *
     * @param object $object
     * @param array $parameters
     * @param null|object $overrideInput
     * @param array $options
     * @return array|\JMS\Serializer\scalar|object|string
     */
    public function get($object, array $parameters = [], $overrideInput = null, array $options = []);

    /**
     * Perform a POST request on an object
     *
     * @param object $object
     * @param array $parameters
     * @param null|object $overrideInput
     * @param array $options
     * @return array|\JMS\Serializer\scalar|object|string
     */
    public function post($object, array $parameters = [], $overrideInput = null, array $options = []);

    /**
     * Perform a PUT request on an object
     *
     * @param object $object
     * @param array $parameters
     * @param null|object $overrideInput
     * @param array $options
     * @return array|\JMS\Serializer\scalar|object|string
     */
    public function put($object, array $parameters = [], $overrideInput = null, array $options = []);

    /**
     * Perform a DELETE request on an object
     *
     * @param object $object
     * @param array $parameters
     * @param null|object $overrideInput
     * @param array $options
     * @return array|\JMS\Serializer\scalar|object|string
     */
    public function delete($object, array $parameters = [], $overrideInput = null, array $options = []);

    /**
     * Perform a PATCH request on an object
     *
     * @param object $object
     * @param array $parameters
     * @param null|object $overrideInput
     * @param array $options
     * @return array|\JMS\Serializer\scalar|object|string
     */
    public function patch($object, array $parameters = [], $overrideInput = null, array $options = []);

    /**
     * Perform a HEAD request on an object
     *
     * @param object $object
     * @param array $parameters
     * @param null|object $overrideInput
     * @param array $options
     * @return array|\JMS\Serializer\scalar|object|string
     */
    public function head($object, array $parameters = [], $overrideInput = null, array $options = []);
}
