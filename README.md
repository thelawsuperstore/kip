# Blinkio - Kip

Kip is an object-mapped REST client Symfony bundle. It allows you to relate domain classes and REST endpoint/methods to 
form a fluid client interface for ORM style transactions.

## Installation

Kip is a bundle for Symfony >= 2.7. To install it, require the package in composer:

```json
"require": {
    "blinkio/kip": "^1.0",
},
"repositories": [
    {
        "type": "git",
        "url": "git@bitbucket.org:thelawsuperstore/kip.git"
    }
]
```

Also, remember to add both the JMS serializer bundle and the Kip bundle to your application kernel:

```php
public function registerBundles()
{
    $bundles = array(
        // ...
        new JMS\SerializerBundle\JMSSerializerBundle(),
        new Blinkio\KipBundle\BlinkioKipBundle(),
    );
    
    // ...
}
```

### Configuration

You can configure Kip from your `config.yml` files like so:

```yaml
# app/config/config.yml
blinkio_kip:
    api_uri_prefix: http://iris-ba.local:8002/app_dev.php/api/rest/v1
    timeout_seconds: 30
    content_type: application/json
    
    # Optional result cache default config
    result_cache:
        cache_service: app.default_result_cache # Service ID to implementation of Doctrine Doctrine\Common\Cache\Cache interface
        default_timeout_seconds: 20
        security_token_aware: true
    
    # Optional mapping config
    mappings:
        - { method: GET, uri: '/partners/{id}.json', class: AppBundle\Model\Partner }
        - { method: POST, uri: '/partners.json', class: AppBundle\Model\Partner }
        - { method: PUT, uri: '/partners/{id}.json', class: AppBundle\Model\Partner }
        - { method: DELETE, uri: '/partners/{id}.json', class: AppBundle\Model\Partner }
```

As a brief explanation of each of the configuration parameters:

 - **api_uri_prefix** - This is the URI prefix prepended before each of the endpoint URIs
 - **timeout_seconds** - How long Kip should wait before giving up on a request
 - **content_type** - The request/response body encoding used for REST transfers
 - **mappings** - A list of endpoint mappings, these map a URI and method to a domain class
 
### Basic Usage

The following examples are related to using the Kip manager service from within a Symfony controller to perform CRUD
operations on a set of domain objects.

**GET**

```php
// Getting a Partner (Calls the GET /partners/{id}.json endpoint)
$object = $this->get('blinkio.kip.manager')->send(new Partner(), 'GET');

dump($object);
```

**POST**

```php
// Posting a Partner (Calls the POST /partners.json endpoint)
$partner = (new Partner())
    ->setName('My New Partner');

$object = $this->get('blinkio.kip.manager')->send($partner, 'POST');

dump($object);
```

**PUT**

```php
// Putting a Partner (Calls the PUT /partners/{id}.json endpoint)
$partner = (new Partner())
    ->setId(1)
    ->setName('My New Partner');

$object = $this->get('blinkio.kip.manager')->send($partner, 'PUT');

dump($object);
```

**DELETE**

```php
// Deleting a Partner (Calls the DELETE /partners/{id}.json endpoint)
// Note that you can also send URI parameters in the last argument of send
$response = $this->get('blinkio.kip.manager')->send(new Partner(), 'DELETE', ['id' => 1]);

dump($response);
```

### HTTP Method Helpers

Instead of specifying the HTTP method in the `send()` method of the manager, you can simply use the following helpers:

```php
$this->get('blinkio.kip.manager')->get($partner);

$this->get('blinkio.kip.manager')->post($partner);

$this->get('blinkio.kip.manager')->put($partner);

$this->get('blinkio.kip.manager')->delete($partner);

$this->get('blinkio.kip.manager')->patch($partner);

$this->get('blinkio.kip.manager')->head($partner);
```

### Result Cache Settings Override

You can override result cache setting by passing options to the send statement, like so:

```php
$result = $this->get('blinkio.kip.manager')->get($partner, [], null, ['result_cache_enabled' => false, 'result_cache_timeout' => 30]);
```

**Note:** This will override the default result cache settings if configured in `app/config/config.yml` 

### Domain Classes

Kip uses [JMS Serializer](http://jmsyst.com/libs/serializer) to serialise objects before they're sent and when they're 
received (where possible). For more information on how to use JMS serializer, please see [the documentation](http://jmsyst.com/libs/serializer).

```php
use Blinkio\KipBundle\Annotation as Kip;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Partner
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Partner
{
    /**
     * @var int
     *
     * @Kip\Id()
     * @Serializer\Expose
     * @Serializer\Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Expose
     * @Serializer\Type("string")
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @Serializer\Expose
     * @Serializer\Type("DateTime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Serializer\Expose
     * @Serializer\Type("DateTime")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
```

**Note** If the class has ID(s) then use the Kip annotation `Blinkio\KipBundle\Annotation\Id` to designate them

### Mapping Configuration via Annotation

Instead of defining the mapping information in `app/config/config.yml` you can also specify mapping in the domain model class, like so:

```php
use Blinkio\KipBundle\Annotation as Kip;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Partner
 *
 * @Kip\Mapping(method="GET", uri="/partners/{id}.json")
 * @Kip\Mapping(method="POST", uri="/partners.json")
 * @Kip\Mapping(method="PUT", uri="/partners/{id}.json")
 * @Kip\Mapping(method="DELETE", uri="/partners/{id}.json")
 * 
 * @Serializer\ExclusionPolicy("all")
 */
class Partner
{
    /**
     * @var int
     *
     * @Kip\Id()
     * @Serializer\Expose
     * @Serializer\Type("integer")
     */
    private $id;
    
    // ...
}
```

### Exception Responses

Upon an HTTP exception Kip will translate and throw these as subclasses of `Blinkio\KipBundle\Exception\Http\AbstractHttpException`. An example use-case is when reading
validation failure errors:

```php
// Posting a Partner (Calls the POST /partners.json endpoint)
$partner = (new Partner())
    ->setName('');

try {
    $object = $this->get('blinkio.kip.manager')->send($partner, 'POST');
} catch (\Blinkio\KipBundle\Exception\Http\ValidationFailureException $e) {
    dump($e->getErrors());
    return;
}

dump($object);
```

### Authentication

If REST requests are authenticated, simply configure an authentication strategy before calling send(). It might look 
something like this:

```php
// Configure an authentication strategy
$this->get('blinkio.kip.manager')->setAuthenticationStrategy(new WsseAuthenticationStrategy([
    'username' => '<username>',
    'key' => '<key>',
]));

// Form a send()
$object = $this->get('blinkio.kip.manager')->send(new Partner(), 'GET');

dump($object);
```

**Note: ** Supported authentication strategies are:

 - WSSE

### Raw Requests

If you need to bypass the class-mapped requests, you can make a raw request like so:

```php
/** @var \Psr\Http\Message\ResponseInterface $response */
$response = $this->get('blinkio.kip.manager')->sendRaw('POST', '/users.json', json_encode(['email' => 'joe.bloggs@example.com']));
```

### Input Override

You can override the input object so that the input and output object types may be different

```php
$input = new MyDifferentPartner();

// Getting a Partner (Calls the GET /partners/{id}.json endpoint)
$object = $this->get('blinkio.kip.manager')->send(new Partner(), 'GET', $input);

dump($object);
```

### Doctrine Associations

On some occasions it is necessary to associate Kip models with [Doctrine](http://www.doctrine-project.org/) entities. 
This is done via the `Association` annotation. To use this, do the following:

Let's say that we have the Doctrine entity:

```php
use Blinkio\KipBundle\Annotation as Kip;

class Page
{
    // ...
    
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    protected $username;

    /**
     * @var User
     *
     * @Kip\Doctrine\Association(
     *     column=@ORM\Column(name="user_id", type="integer", length=11),
     *     parameterMapping={"user"="id"},
     *     targetClass="AppBundle\Model\User",
     *     operations={"load"="GET"},
     *     authenticationStrategy="app.dummy_wsse_auth_strategy",
     *     isResultCacheEnabled=true,
     *     resultCacheTimeout=10
     * )
     */
    protected $user;

    /**
     * @var ArrayCollection
     *
     * @Kip\Doctrine\Association(
     *     parameterMapping={"username"="username"},
     *     targetClass="AppBundle\Model\Collection",
     *     operations={"load"="GET"},
     *     authenticationStrategy="app.dummy_wsse_auth_strategy",
     *     isResultCacheEnabled=false
     * )
     */
    protected $users;
    
    // ...
}
```

As you can see, when the doctrine entity is loaded, the associated Kip models are also loaded. Here's a breakdown of the
annotation configuration for `Association`:

 - **column** - If the entity is to hold a FK to a REST service then the column must be defined here
 - **targetClass** - The target Kip model that should be hydrated
 - **operations** - An array of operations to perform (related to Doctrine lifecycle events to HTTP methods)
 - **parameterMapping** - Map properties of this Doctrine entity to HTTP URI/Query parameters
 - **authenticationStrategy** - Service ID of the authentication strategy to use for Kip requests
 - **isResultCacheEnabled** - Is the result cache enabled for this association? (optional - uses default result cache setting if not specified)
 - **resultCacheTimeout** - Result cache timeout (if enabled) (optional - uses default result cache setting if not specified)
 